package de.sb.broker.model;

import static org.junit.Assert.*;

import javax.validation.Validator;

import org.junit.Test;

import de.htw.prog.broker.model.Address;

public class AddressEntityTest extends EntityTest {
	

	@Test
	public void testConstraints() {
		Validator validator = this.getEntityValidatorFactory().getValidator();
		
		
		// create valid address
		Address address = new Address();

		address.setStreet("legal street");
		assertEquals("legal street", address.getStreet());
		
		
		address.setPostCode("12345");
		assertEquals("12345", address.getPostCode());
		
		
		address.setCity("legal city");
		assertEquals("legal city", address.getCity());
		
		// validate legal object
		assertEquals(0, validator.validate(address).size());
		


		// tests for street
		address.setStreet(null);
		assertEquals(0, validator.validate(address).size());

		// still legal values
		address.setStreet("");
		assertEquals(0, validator.validate(address).size());
		
		address.setStreet(stringOfLength(63));
		assertEquals(0, validator.validate(address).size());
		
		// illegal values
		address.setStreet(stringOfLength(64));
		assertEquals(1, validator.validate(address).size());
		
		address.setStreet("legal street");
		
		// tests for postCode
		// still legal values
		address.setPostCode(null);
		assertEquals(0, validator.validate(address).size());

		address.setPostCode("");
		assertEquals(0, validator.validate(address).size());
		
		address.setPostCode(stringOfLength(15));
		assertEquals(0, validator.validate(address).size());

		// illegal values
		address.setPostCode(stringOfLength(16));
		assertEquals(1, validator.validate(address).size());
		
		address.setPostCode("postcode");
		
		// tests for city
		address.setCity(null);
		assertEquals(0, validator.validate(address).size());

		// still legal values
		address.setCity("X");
		assertEquals(0, validator.validate(address).size());
		
		address.setCity(stringOfLength(63));
		assertEquals(0, validator.validate(address).size());
		
		// illegal values
		address.setCity("");
		assertEquals(1, validator.validate(address).size());

		address.setCity(stringOfLength(64));
		assertEquals(1, validator.validate(address).size());
		

		
		
		
	}
	
	@Test
	public void testLifeCycle() {
		
	}
}
