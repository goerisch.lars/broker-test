package de.sb.broker.model;

import static org.junit.Assert.*;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.Test;

import de.htw.prog.broker.model.Address;
import de.htw.prog.broker.model.Auction;
import de.htw.prog.broker.model.Bid;
import de.htw.prog.broker.model.Contact;
import de.htw.prog.broker.model.Name;
import de.htw.prog.broker.model.Person;
import de.htw.prog.broker.model.Person.Group;

public class AuctionEntityTest extends EntityTest {

	@Test
	public void testConstraints() {

		Validator validator = this.getEntityValidatorFactory().getValidator();

		Person seller = new Person();
		Person bidder = new Person();

		Auction auction = new Auction(seller);
		auction.setUnitCount((short) 1);
		auction.setDescription("description");

		Bid bid = new Bid(auction, bidder);

		auction.getBids().add(bid);

		Bid[] bids = { bid };

		assertArrayEquals(bids, auction.getBids().toArray());

		auction.setAskingPrice(0);
		assertEquals(0, bid.getPrice());

		auction.setTitle("title");
		assertEquals("title", auction.getTitle());

		assertEquals(bid, auction.getBid(bidder));
		assertEquals(1, auction.getBids().size());

		auction.setClosureTimestamp(1);
		assertEquals(1, auction.getClosureTimestamp());

		// title
		auction.setTitle("X");
		assertEquals(0, validator.validate(auction).size());

		auction.setTitle(stringOfLength(255));
		assertEquals(0, validator.validate(auction).size());

		auction.setTitle(stringOfLength(0));
		assertEquals(1, validator.validate(auction).size());

		auction.setTitle(stringOfLength(256));
		assertEquals(1, validator.validate(auction).size());

		auction.setTitle("title");

		// unitCount
		auction.setUnitCount((short) 1);
		assertEquals(0, validator.validate(auction).size());

		auction.setUnitCount((short) 0);
		assertEquals(1, validator.validate(auction).size());

		auction.setUnitCount((short) 1);

		// askingPrice
		auction.setAskingPrice(0);
		assertEquals(0, validator.validate(auction).size());

		auction.setAskingPrice(-1);
		assertEquals(1, validator.validate(auction).size());

		auction.setAskingPrice(0);

		// description
		auction.setDescription("X");
		assertEquals(0, validator.validate(auction).size());

		auction.setDescription(stringOfLength(8189));
		assertEquals(0, validator.validate(auction).size());

		auction.setDescription("");
		assertEquals(1, validator.validate(auction).size());

		auction.setDescription(stringOfLength(8190));
		assertEquals(1, validator.validate(auction).size());

		auction.setDescription("description");

		// test isClosed
		auction.setClosureTimestamp(System.currentTimeMillis() - 1000);
		assertEquals(true, auction.isClosed());

		assertEquals(true, auction.isSealed());

	}

	@Test
	public void testLifeCycle() {
		EntityManager brokerManager = this.getEntityManagerFactory()
				.createEntityManager();
		try{
			
		
		brokerManager.getTransaction().begin();

		final Person seller = new Person("seller2",
				Person.passwordHash("test233w"), new Name(), new Address(),
				new Contact());

		seller.getAddress().setCity("testCity");
		seller.getAddress().setPostCode("12345");
		seller.getAddress().setStreet("terstStreet");

		seller.getName().setFamily("testfamily");
		seller.getName().setGiven("given");

		seller.getContact().setEmail("mail@test.de");
		seller.getContact().setPhone("123456");

		seller.setGroup(Group.ADMIN);

		brokerManager.persist(seller);
		brokerManager.getTransaction().commit();
		brokerManager.getTransaction().begin();

		brokerManager.getReference(seller.getClass(), seller.getIdentity());
		Auction auction = new Auction(brokerManager.find(seller.getClass(), seller.getIdentity()));
		auction.setAskingPrice(2);
		auction.setUnitCount((short) 10);
		auction.setTitle("neue Auction2");
		auction.setDescription("Toller artikel2");
		auction.setClosureTimestamp(auction.getClosureTimestamp() + 1000);
		brokerManager.persist(auction);

		
		brokerManager.remove(auction);
		brokerManager.remove(brokerManager.getReference(seller.getClass(), seller.getIdentity()));
		brokerManager.getTransaction().commit();
	//	brokerManager.flush();
		}catch (Exception e){
			e.printStackTrace();
			brokerManager.getTransaction().rollback();
			brokerManager.close();
		}finally{
			brokerManager.close();
		}
	

	}
}