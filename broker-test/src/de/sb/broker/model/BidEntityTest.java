package de.sb.broker.model;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.validation.Validator;

import org.junit.Test;

import de.htw.prog.broker.model.Address;
import de.htw.prog.broker.model.Auction;
import de.htw.prog.broker.model.Bid;
import de.htw.prog.broker.model.Contact;
import de.htw.prog.broker.model.Name;
import de.htw.prog.broker.model.Person;
import de.htw.prog.broker.model.Person.Group;

public class BidEntityTest extends EntityTest {

	@Test
	public void testConstraints() {

		Validator validator = this.getEntityValidatorFactory().getValidator();

		Person seller = new Person();
		Person bidder = new Person();

		Auction auction = new Auction(seller);

		Bid bid = new Bid(auction, bidder);

		// price setter
		bid.setPrice(1);
		assertEquals(1, bid.getPrice());

		assertEquals(auction, bid.getAuction());
		assertEquals(auction.getIdentity(), bid.getAuctionReference());

		assertEquals(bidder, bid.getBidder());
		assertEquals(bidder.getIdentity(), bid.getBidderReference());

	}

	@Test
	public void testLifeCycle() {
		EntityManager brokerManager = this.getEntityManagerFactory()
				.createEntityManager();
		try{		
		
		brokerManager.getTransaction().begin();

		final Person seller = new Person("seller3",
				Person.passwordHash("test233w"), new Name(), new Address(),
				new Contact());

		seller.getAddress().setCity("testCity");
		seller.getAddress().setPostCode("12345");
		seller.getAddress().setStreet("terstStreet");

		seller.getName().setFamily("testfamily");
		seller.getName().setGiven("given");

		seller.getContact().setEmail("mail@test.de");
		seller.getContact().setPhone("123456");

		seller.setGroup(Group.ADMIN);

		brokerManager.persist(seller);
		
		final Person bidder = new Person("bidder1",
				Person.passwordHash("test233w"), new Name(), new Address(),
				new Contact());

		bidder.getAddress().setCity("testCity");
		bidder.getAddress().setPostCode("12345");
		bidder.getAddress().setStreet("terstStreet");

		bidder.getName().setFamily("testfamily");
		bidder.getName().setGiven("given");

		bidder.getContact().setEmail("bidder@test.de");
		bidder.getContact().setPhone("123456");

		bidder.setGroup(Group.ADMIN);

		brokerManager.persist(bidder);
		
		brokerManager.getTransaction().commit();
		brokerManager.getTransaction().begin();

		brokerManager.getReference(seller.getClass(), seller.getIdentity());
		Auction auction = new Auction(brokerManager.find(seller.getClass(),
				seller.getIdentity()));
		auction.setAskingPrice(2);
		auction.setUnitCount((short) 10);
		auction.setTitle("neue Auction3");
		auction.setDescription("Toller artikel3");
		auction.setClosureTimestamp(auction.getClosureTimestamp() + 1000);
		brokerManager.persist(auction);

		brokerManager.getTransaction().commit();
		brokerManager.getTransaction().begin();
		
		brokerManager.getReference(seller.getClass(), seller.getIdentity());
		brokerManager.getReference(auction.getClass(), auction.getIdentity());
		brokerManager.getReference(bidder.getClass(), bidder.getIdentity());
		
		Bid bid = new Bid(brokerManager.find(auction.getClass(),
				auction.getIdentity()), brokerManager.find(bidder.getClass(),
						bidder.getIdentity()) );
		bid.setPrice(5);
		brokerManager.persist(bid);
		brokerManager.remove(auction);
		brokerManager.remove(brokerManager.getReference(seller.getClass(), seller.getIdentity()));	
		brokerManager.remove(bidder);
		brokerManager.remove(bid);
		brokerManager.getTransaction().commit();
	//	brokerManager.flush();
		}catch (Exception e){
			e.printStackTrace();
			brokerManager.getTransaction().rollback();
			brokerManager.close();
		}finally{
			brokerManager.close();
		}
		
		

		
	}
}
