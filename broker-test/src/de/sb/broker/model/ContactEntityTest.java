package de.sb.broker.model;

import static org.junit.Assert.*;

import javax.validation.Validator;

import org.junit.Test;

import de.htw.prog.broker.model.Contact;

public class ContactEntityTest extends EntityTest {

	@Test
	public void testConstraints() {
        Validator validator = this.getEntityValidatorFactory().getValidator();
        
        Contact contact = new Contact();

        contact.setEmail("valid@valid.org");
        assertEquals("valid@valid.org", contact.getEmail());
        
        contact.setPhone("12345");
        assertEquals("12345", contact.getPhone());
        
        // tests for email
        
        // valid
        contact.setEmail("a@b.org");
        assertEquals(0, validator.validate(contact).size());

        contact.setEmail(stringOfLength(58) + "@b.de");
        assertEquals(0, validator.validate(contact).size());
        
        // length => 1 makes no sense du to regex
        
        contact.setEmail("foofoob.de");
        assertEquals(1, validator.validate(contact).size());
        
        contact.setEmail(null);
        assertEquals(1, validator.validate(contact).size());

        // reset email
        contact.setEmail("x@b.de");


        // tests for phone
        // valid
        contact.setPhone("");
        assertEquals(0, validator.validate(contact).size());

        contact.setPhone(stringOfLength(63));
        assertEquals(0, validator.validate(contact).size());

        contact.setPhone(stringOfLength(64));
        assertEquals(1, validator.validate(contact).size());
        
	}
}
