package de.sb.broker.model;

import static org.junit.Assert.*;

import javax.validation.Validator;

import org.junit.Test;

import de.htw.prog.broker.model.Contact;
import de.htw.prog.broker.model.Name;

public class NameEntityTest extends EntityTest {

	@Test
	public void testConstraints() {
        Validator validator = this.getEntityValidatorFactory().getValidator();
        
        Name name = new Name();

        
        name.setFamily("legal family");
        assertEquals("legal family", name.getFamily());
        
        name.setGiven("legal given");
        assertEquals("legal given", name.getGiven());
        
        
        // tests for family
        // valid
        name.setFamily("X");
        assertEquals(0, validator.validate(name).size());
        
        name.setFamily(stringOfLength(31));
        assertEquals(0, validator.validate(name).size());
        
        // invalid
        
        name.setFamily("");
        assertEquals(1, validator.validate(name).size());

        name.setFamily(stringOfLength(32));
        assertEquals(1, validator.validate(name).size());

        // reset family
        name.setFamily("legal family");

        // tests for given
        // valid
        name.setGiven("X");
        assertEquals(0, validator.validate(name).size());
        
        name.setGiven(stringOfLength(31));
        assertEquals(0, validator.validate(name).size());
        
        // invalid
        
        name.setGiven("");
        assertEquals(1, validator.validate(name).size());

        name.setGiven(stringOfLength(32));
        assertEquals(1, validator.validate(name).size());

        // reset given
        name.setGiven("legal given");

        
	}
}
