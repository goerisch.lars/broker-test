package de.sb.broker.model;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.junit.Test;

import com.mysql.fabric.xmlrpc.base.Array;

import de.htw.prog.broker.model.Address;
import de.htw.prog.broker.model.Auction;
import de.htw.prog.broker.model.Bid;
import de.htw.prog.broker.model.Contact;
import de.htw.prog.broker.model.Name;
import de.htw.prog.broker.model.Person;
import de.htw.prog.broker.model.Person.Group;

public class PersonEntityTest extends EntityTest {
	
	@Test
	public void testConstraints(){
		
		Validator validator = this.getEntityValidatorFactory().getValidator();
		
		Name name = new Name();
		Address address = new Address();
		Contact contact = new Contact();


		Person person = new Person("alias", Person.passwordHash("password"), name, address, contact); 

		Auction auction = new Auction(person);
		Bid bid = new Bid(auction, person);
		

		// alias getter
		assertEquals("alias", person.getAlias());
		
		
		// group getter
		// Person.Group is not visible...
		person.setGroup(Person.Group.ADMIN);
		assertEquals(Person.Group.ADMIN, person.getGroup());

		// wont work (constructor assigns new objets)
//		assertEquals(name, person.getName());
//		assertEquals(address, person.getAddress());
//		assertEquals(name, person.getName());
		
		// what about bids and auctions?
		
		// alias
		Person person1 = new Person("x", Person.passwordHash("password"), name, address, contact);
        assertEquals(0, validator.validate(person1).size());
	
		person1 = new Person(stringOfLength(16), Person.passwordHash("password"), name, address, contact);
        assertEquals(0, validator.validate(person1).size());

		person1 = new Person("", Person.passwordHash("password"), name, address, contact);
        assertEquals(1, validator.validate(person1).size());
	  
		person1 = new Person(stringOfLength(17), Person.passwordHash("password"), name, address, contact);
        assertEquals(1, validator.validate(person1).size());
	}

	@Test
	public void testLifeCycle(){
		EntityManager brokerManager = this.getEntityManagerFactory()
				.createEntityManager();
		try{
			
		brokerManager.getTransaction().begin();

		final Person seller = new Person("seller1",
				Person.passwordHash("test233w"), new Name(), new Address(),
				new Contact());

		seller.getAddress().setCity("testCity");
		seller.getAddress().setPostCode("12345");
		seller.getAddress().setStreet("terstStreet");

		seller.getName().setFamily("testfamily");
		seller.getName().setGiven("given");

		seller.getContact().setEmail("mail@test.de");
		seller.getContact().setPhone("123456");

		seller.setGroup(Group.ADMIN);

		brokerManager.persist(seller);
	
		brokerManager.remove(seller);
		brokerManager.getTransaction().commit();
	//	brokerManager.flush();
		}catch (Exception e){
			e.printStackTrace();
			brokerManager.getTransaction().rollback();
			brokerManager.close();
		}finally{
			brokerManager.close();
		}
	}
}
